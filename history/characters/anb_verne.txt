﻿
bronzewing_0001 = { # Artur of Bronzewing, Duke of Wyvernmark
	name = "Artur"
	dynasty = dynasty_bronzewing
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_3
	trait = temperate
	trait = content
	trait = gregarious
	# trait = unyielding_defender
	
	932.6.14 = {
		birth = yes
	}
	
	999.12.29 = {
		death = "999.12.29"
	}
}

bronzewing_0002 = { # Vurcan of Bronzewing
	name = "Vurcan"
	dynasty = dynasty_bronzewing
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = stubborn
	trait = compassionate
	trait = twin
	trait = unyielding_defender
	
	father = bronzewing_0001
	
	970.3.28 = {
		birth = yes
	}
	
	990.8.20 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

bronzewing_0003 = { # Vercan of Bronzewing, Duke of Wyvernmark
	name = "Vercan"
	dynasty = dynasty_bronzewing
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_2
	trait = diligent
	trait = content
	trait = chaste
	trait = twin
	trait = unyielding_defender
	
	father = bronzewing_0001
	
	970.3.28 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	1001.5.19 = {
		add_spouse = vernid_0001
	}
}

bronzewing_0004 = { # Arman of Bronzewing
	name = "Arman"
	dynasty = dynasty_bronzewing
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_diplomacy_2
	trait = temperate
	trait = humble
	trait = gregarious
	
	father = bronzewing_0003
	mother = vernid_0001
	
	1004.9.10 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

bronzewing_0005 = {
	name = "Valenza"
	dynasty = dynasty_bronzewing
	religion = vernid_adean
	culture = vernid
	female = yes
	
	trait = race_human
	
	father = bronzewing_0001
	
	972.1.17 = {
		birth = yes
	}
}

tails_end_0001 = { # Waltheri of Tail's End, Duke of the Tail
	name = "Waltheri"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_stewardship_2
	trait = vengeful
	trait = greedy
	trait = callous
	trait = reclusive
	
	951.11.20 = {
		birth = yes
	}
	
	990.10.12 = {
		effect = {
			add_pressed_claim = title:c_stingport
		}
	}
}

tails_end_0002 = { # Armocan of Tail's End, son of Waltheri
	name = "Armocan"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_1
	trait = content
	trait = honest
	trait = greedy
	
	father = tails_end_0001
	
	970.7.30 = {
		birth = yes
	}
	
	994.12.4 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

tails_end_0003 = { # Vernell of Tail's End, son of Armocan
	name = "Vernell"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_2
	trait = vengeful
	trait = wrathful
	trait = patient
	
	father = tails_end_0002
	
	989.4.22 = {
		birth = yes
	}
	
	994.12.4 = {
		set_relation_guardian = character:tails_end_0001
	}
	
	1005.4.22 = {
		remove_relation_guardian = character:tails_end_0001
	}
	
	1008.6.23 = {
		# add_spouse = daughter of armhal count/duke
	}
	
	1022.1.1 = {
		effect = {
			add_pressed_claim = title:c_stingport # he would not inherit it from his grandfather
		}
	}
}

tails_end_0004 = { # Armoc of Tail's End, son of Vernell
	name = "Armoc"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = rowdy
	trait = arbitrary
	trait = stubborn
	disallow_random_traits = yes
	
	father = tails_end_0003
	
	1009.11.10 = {
		birth = yes
	}
}

tails_end_0005 = { # Tegan of Tail's End, son of Waltheri
	name = "Tegan"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_diplomacy_3
	trait = gregarious
	trait = deceitful
	trait = lustful
	
	father = tails_end_0001
	
	972.10.7 = {
		birth = yes
	}
	
	996.7.5 = {
		death = {
			death_reason = death_ill
		}
	}
}

tails_end_0006 = { # Alvar of Tail's End, son of Waltheri
	name = "Alvar"
	dynasty = dynasty_tails_end
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_diplomacy_1
	trait = gregarious
	trait = trusting
	trait = craven
	trait = depressed_1
	
	father = tails_end_0001
	
	976.2.10 = {
		birth = yes
	}
	
	997.5.19 = {
		death = {
			death_reason = death_suicide
		}
	}
}

gagliardid_0001 = { # Roger Gagliardid, Count of Napesbay and Watchman's Point
	name = "Roger"
	dynasty = dynasty_gagliardid
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_martial_2
	trait = stubborn
	trait = wrathful
	trait = patient
	trait = loyal
	trait = cautious_leader
	
	954.10.7 = {
		birth = yes
	}
	
	980.1.7 = {
		add_spouse = cliffman_0007
	}
	
	1022.1.1 = {
		set_primary_title_to = title:c_napesbay # cooler titles
	}
}

gagliardid_0002 = { # Otto Gagliardid
	name = "Otto"
	dynasty = dynasty_gagliardid
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_learning_1
	trait = eccentric
	trait = impatient
	trait = trusting
	
	father = gagliardid_0001
	mother = cliffman_0007
	
	982.4.18 = {
		birth = yes
	}
	
	996.12.8 = { # went with his father to inspect a new ship, started playing on the mast, slipped, fell and hit his head
		trait = incapable
	}
}

gagliardid_0003 = { # Aucán Gagliardid
	name = "AucA_n"
	dynasty = dynasty_gagliardid
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_stewardship_3
	trait = temperate
	trait = chaste
	trait = compassionate
	
	father = gagliardid_0001
	mother = cliffman_0007
	
	987.9.17 = {
		birth = yes
	}
	
	996.12.8 = {
		effect = {
			set_relation_best_friend = character:gagliardid_0002 # really cares about his brother
		}
	}
}

gagliardid_0004 = { # Lorens Gagliardid
	name = "Lorens"
	dynasty = dynasty_gagliardid
	religion = vernid_adean
	culture = vernid
	
	trait = race_human
	trait = education_diplomacy_2
	trait = arbitrary
	trait = lazy
	trait = gregarious
	
	father = gagliardid_0003
	
	1005.6.14  ={
		birth = yes
	}
}

######### Minor Characters #########

vernid_0001 = { # Maria, wife of Vercan of Bronzewing #TODO: change this ID (she's not a Vernid dynasty) and fix the others (would be cringe to start at 0002)
	name = "Maria"
	# lowborn
	religion = cult_of_the_dame
	culture = vernman
	female = yes
	
	977.10.5 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

vernid_0002 = {
	name = "Rocair"
	dna = 48_rocair_vernid
	dynasty = dynasty_vernid #Vernid
	religion = "vernid_adean"
	culture = "vernid"
	father = vernid_0003
	mother = vernid_0006
	
	trait = education_martial_4
	trait = flexible_leader
	trait = brave
	trait = patient
	trait = just
	trait = physique_good_2
	trait = scarred
	trait = whole_of_body
	trait = gallant
	trait = race_human
	
	970.3.4 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
}

vernid_0003 = {
	name = "Arman"	#Father of the Three Wyvern Kings
	dynasty = dynasty_vernid
	religion = "vernid_adean"
	culture = "vernid"	

	trait = education_martial_4
	trait = brave
	trait = gregarious
	trait = stubborn
	trait = physique_good_2

	trait = race_human

	950.6.2 = {
		birth = yes
	}

	969.12.5 = {
		add_spouse = vernid_0006
	}

	989.6.30 = { # Slaughter of Cronesford, during Invasion of East Dameshead
		death = {	
			death_reason = death_killed_war_of_sorcerer_king
			killer = 514	#Iacob the Betrayer
		}
	}
}

vernid_0004 = {
	name = "Maurisio"	#Maurisio, King of Menibor
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0003
	mother = vernid_0006

	trait = education_stewardship_4
	trait = ambitious
	trait = gregarious
	trait = eccentric
	trait = physique_good_2

	trait = race_human

	diplomacy = 8
	stewardship = 10

	980.11.8 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	1004.10.6 = {
		add_spouse = vernid_0015
	}
}

vernid_0005 = {
	name = "Emil"	#Emil, King of Armanhal
	dynasty = dynasty_vernid
	religion = "vernid_adean"
	culture = "vernid"	
	father = vernid_0003
	mother = vernid_0006

	trait = education_diplomacy_4
	trait = diligent
	trait = honest
	trait = cynical
	trait = physique_good_2

	trait = race_human

	982.3.25 = {
		birth = yes
	}
	996.12.16= {
		add_spouse = cliffman_0006
	}
}

vernid_0006 = {
	name = "Gullamina"	#First Wife of Arman
	#dynasty = dynasty_vernid
	religion = "vernid_adean"
	culture = "vernid"	
	female = yes
	trait = education_martial_2

	trait = race_human

	951.3.12 = {
		birth = yes
	}

}

vernid_0007 = {
	name = "LiC_oleta"	#Second Wife of Arman
	#dynasty = dynasty_vernid
	religion = "vernid_adean"
	culture = "esmari"	
	female = yes
	trait = education_diplomacy_3
	
	trait = race_human

	980.11.8 = {
		birth = yes
	}

}

vernid_0008 = {
	name = "Vacho"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	

	trait = education_martial_3
	trait = brave
	trait = lustful
	trait = arrogant

	trait = race_human

	984.10.1 = {
		birth = yes
	}

	1005.1.4 = {
		give_nickname = nick_greatsting
	}
}

#Greatsting bastards
vernid_0009 = {
	name = "Cailen"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008

	trait = bastard

	trait = race_human

	1004.12.23 = {
		birth = yes
	}
}

vernid_0010 = {
	name = "Vacha"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008
	female = yes

	trait = bastard

	trait = race_half_elf

	1007.4.14 = {
		birth = yes
	}
}

vernid_0011 = {
	name = "Brando"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008

	trait = bastard

	trait = race_human

	1009.1.10 = {
		birth = yes
	}
}

vernid_0012 = {
	name = "Madalena"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008
	female = yes

	trait = bastard

	trait = race_human

	1011.2.25 = {
		birth = yes
	}
}

vernid_0013 = {
	name = "Lotario"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008

	trait = bastard

	trait = race_human

	1012.4.16 = {
		birth = yes
	}
}

vernid_0014 = {
	name = "Ana"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0008
	female = yes

	trait = bastard

	trait = race_half_elf

	1011.3.18 = {
		birth = yes
	}
}

vernid_0015 = {	#wife of Maurisio
	name = "Lisabeth"
	dynasty = dynasty_carstenard
	religion = "luna_damish"
	culture = "wexonard"	
	female = yes

	trait = education_intrigue_2
	trait = vengeful
	trait = greedy
	trait = cynical

	father = carstenard_0002
	mother = bronzewing_0005

	trait = race_human

	985.6.24 = {
		birth = yes
	}
}

vernid_0016 = {	#son of Maurisio
	name = "Maurisio"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	father = vernid_0004
	mother = vernid_0015

	trait = charming
	# trait = greedy
	# trait = content
	# trait = shy

	trait = race_human

	1007.2.14 = {
		birth = yes
	}
}


vernid_0017 = {	#daughter of Maurisio
	name = "Gullamina"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	female = yes
	father = vernid_0004
	mother = vernid_0015

	trait = rowdy
	# trait = brave
	# trait = generous
	# trait = stubborn

	trait = race_human

	1009.2.16 = {
		birth = yes
	}
}

vernid_0018 = {	#daughter of Emil
	name = "Isabella"
	dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "vernman"	
	female = yes
	father = vernid_0005
	mother = cliffman_0006

	trait = lustful
	trait = compassionate
	trait = impatient
	trait = education_diplomacy_2
	trait = physique_good_2

	trait = race_human

	997.2.10 = {
		birth = yes
	}
}

laudaris_0001 = {	#Laudari of Galeinn, Saltmarch
	name = "Laudari"
	dynasty = dynasty_laudaris
	religion = "luna_damish"
	culture = "vernman"	
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = temperate
	trait = honest
	trait = gallant
	trait = unyielding_defender

	974.5.23 = {
		birth = yes
	}
	1003.8.22 = {
		add_spouse = laudaris_0002
	}
}

laudaris_0002 = {
	name = "Cecille"	#Wife of Laudari
	#dynasty = dynasty_vernid
	religion = "luna_damish"
	culture = "adeanic"	
	female = yes
	trait = education_stewardship_2
	
	trait = race_human

	978.10.15 = {
		birth = yes
	}
	1003.8.22 = {
		add_spouse = laudaris_0001
	}
}

laudaris_0003 = {	
	name = "TomA_"	#Son of Laudari
	dynasty = dynasty_laudaris
	religion = "luna_damish"
	culture = "vernman"	
	father = laudaris_0001
	mother = laudaris_0002

	trait = education_stewardship_3

	trait = race_human

	1001.9.5 = {
		birth = yes
	}
	1019.2.24 = {
		add_spouse = laudaris_0004
	}
}

laudaris_0004 = {
	name = "RiannO_n"	#Wufe of Tomá
	#dynasty = dynasty_laudaris
	religion = "luna_damish"
	culture = "damerian"	
	female = yes

	trait = education_diplomacy_2

	trait = race_human

	1000.10.28 = {
		birth = yes
	}
	1019.2.24 = {
		add_spouse = laudaris_0003
	}
}

laudaris_0005 = {	
	name = "Galin"	#Grandson of Laudari, becomes Duke of Galeinn later
	dynasty = dynasty_laudaris
	religion = "luna_damish"
	culture = "vernman"	
	father = laudaris_0003
	mother = laudaris_0004

	trait = race_human

	1021.2.18 = {
		birth = yes
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny	#canonically this guy forms duchy of galeinn
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
	}
}


walterid_0001 = {
	name = "Aven"	#Aven the Craven
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"

	trait = education_diplomacy_1

	trait = craven
	trait = greedy
	trait = content
	trait = family_first

	trait = race_human

	trait = fecund

	959.4.27 = {
		birth = yes
	}

	1000.3.25 = {
		give_nickname = nick_craven
	}

	990.8.22 = {
		add_spouse = walterid_0002
	}

}

walterid_0002 = {
	name = "Carina"
	#dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	female = yes

	trait = education_diplomacy_3

	trait = race_human

	967.2.13 = {
		birth = yes
	}
	990.8.22 = {
		add_spouse = walterid_0001
	}

}

walterid_0003 = {	
	name = "Aven"
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	father = walterid_0001
	mother = walterid_0002

	trait = education_martial_3

	trait = fecund

	trait = race_human

	996.10.3 = {
		birth = yes
	}

}

walterid_0004 = {	#rest of walterid family is for usage by others if needed
	name = "Federic"	
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	father = walterid_0001
	mother = walterid_0002

	trait = education_learning_2

	trait = race_human

	999.4.30 = {
		birth = yes
	}

}

walterid_0005 = {
	name = "Costanza"
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	female = yes
	father = walterid_0001
	mother = walterid_0002

	trait = education_diplomacy_4

	trait = race_human

	trait = twin

	trait = fecund

	1003.12.2 = {
		birth = yes
	}

}

walterid_0006 = {
	name = "Valenza"
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	female = yes
	father = walterid_0001
	mother = walterid_0002

	trait = education_stewardship_4

	trait = twin

	trait = fecund

	trait = race_human

	1003.12.2 = {
		birth = yes
	}

}

walterid_0007 = {
	name = "RimannO_n"
	dynasty = dynasty_walterid
	religion = "luna_damish"
	culture = "vernid"
	female = yes
	father = walterid_0001
	mother = walterid_0002

	trait = education_diplomacy_2

	trait = race_human

	992.12.2 = {
		birth = yes
	}
	1009.3.12 = {
		add_spouse = nahilnis_0003
	}

}

treun_0001 = {
	name = "Anzaro"	
	dna = anzaro_treun
	dynasty = dynasty_treun
	religion = "luna_damish"
	culture = "vernman"	

	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = honest
	trait = gregarious
	trait = shrewd
	trait = scarred
	trait = loyal
	trait = education_martial_prowess_3
	trait = open_terrain_expert

	

	987.8.30 = {
		birth = yes
	}
	998.2.14 = {
		set_relation_friend = character:vernid_0004
	}
	1021.12.25 = {
		add_spouse = treun_0002
	}
}

treun_0002 = {
	name = "Ariatha"
	# lowborn
	religion = elven_forebears
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = education_martial_2
	trait = arrogant
	trait = compassionate
	trait = honest
	
	fertility = 100
	
	893.1.18 = {
		birth = yes
	}
	
	956.3.25 = {
		give_nickname = nick_swiftstep
		effect = {
			set_relation_friend = character:19
		}
	}
	
	1021.12.25 = {
		add_spouse = treun_0001
	}
	
	1021.12.26 = {
		effect = {
			make_pregnant = {
				father = character:treun_0001
			}
		}
	}
}

plainsby_0001 = {
	name = "Ostin"
	dynasty = dynasty_plainsby
	religion = "luna_damish"
	culture = "vernman"	
	
	trait = race_human
	trait = education_martial_3
	trait = zealous
	trait = sadistic
	trait = wrathful
	trait = beauty_bad_3
	trait = adventurer

	995.12.15 = {
		birth = yes
	}
}
