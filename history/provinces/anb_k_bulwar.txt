#k_bulwar
##d_bulwar
###c_bulwar
601 = {		#Bulwar

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = castle_holding
	duchy_capital_building = bulwar_waterworks_01
    # History
}

6574 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

6575 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

6576 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

6577 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

###c_eduz_baranun
602 = {		#Eduz-Baranun

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = church_holding

    # History
}

6578 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

6579 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

6580 = {		#

    # Misc
    culture = zanite
    religion = jaherian_cults
	holding = none
    # History
}

##d_kalib
###c_kalib
599 = {		#Kalib

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6570 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6571 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6572 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6573 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

###c_azanerdu
600 = {		#Azanerdu

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6567 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6568 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6569 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}