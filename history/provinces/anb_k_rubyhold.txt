#k_rubyhold
##d_rubyhold
###c_rubyhold
62 = {		#Rubyhold

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}
1232 = {

    # Misc
    holding = none

    # History

}

4072 = {

    # Misc
    holding = none

    # History

}

4076 = {

    # Misc
    holding = church_holding

    # History

}

4075 = {

    # Misc
    holding = city_holding

    # History

}


###c_rivergate
65 = {		#Rivergate

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}
1231 = {

    # Misc
    holding = none

    # History

}

###c_redgate
64 = {		#Redgate

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}
1230 = {

    # Misc
    holding = city_holding

    # History

}

###c_ruby depths 
4070 = {		#Rubydepths 

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}

4071 = {

    # Misc
    holding = none

    # History

}

###c_rose mines
4073 = {		#Rubydepths 

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}

4074 = {

    # Misc
    holding = none

    # History

}

###c_gempath
4077 = {		#gempath  

    # Misc
    culture = ruby_dwarvish
    religion = dwarven_pantheon
	holding = castle_holding

    # History

}

4078 = {

    # Misc
    holding = none

    # History

}