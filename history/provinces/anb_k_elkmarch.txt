#k_elkmarch
##d_elkmarch
###c_elkwood
206 = {		#Elkwood

    # Misc
    culture = oakfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1194 = {

    # Misc
    holding = city_holding

    # History

}
1195 = {

    # Misc
    holding = none

    # History

}
1198 = {

    # Misc
    holding = none

    # History

}
1200 = {

    # Misc
    holding = none

    # History

}

###c_hardoaks
7 = {		#Hardoaks
	
	# Misc
	culture = oakfoot_halfling
	religion = small_temple
	holding = castle_holding
	
	# History
}
1196 = {

    # Misc
    holding = church_holding

    # History

}
1197 = {

    # Misc
    holding = none

    # History

}

###c_twofork
348 = {		#Twofork

    # Misc
    culture = oakfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1199 = {

    # Misc
    holding = city_holding

    # History

}

###c_the_approach
210 = {		#The Approach

    # Misc
    culture = oakfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1193 = {

    # Misc
    holding = none

    # History

}

##d_uelaire
###c_uelaire
207 = {		#Uelaire

    # Misc
    culture = old_damerian
    religion = cult_of_uelos
	holding = castle_holding

    # History

}
1187 = {

    # Misc
    holding = city_holding

    # History

}
1188 = {

    # Misc
    holding = none

    # History

}
1189 = {

    # Misc
    holding = church_holding

    # History

}

###c_westmere
208 = {		#Westmere

    # Misc
    culture = oakfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1186 = {

    # Misc
    holding = none

    # History

}

###c_highwind
209 = {		#Highwind

    # Misc
    culture = oakfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1190 = {

    # Misc
    holding = none

    # History

}
1201 = {

    # Misc
    holding = city_holding

    # History

}

###c_newacre
347 = {		#Newacre

    # Misc
    culture = oakfoot_halfling
    religion = small_temple

    # History

}
1191 = {

    # Misc
    holding = church_holding

    # History

}
1192 = {

    # Misc
    holding = none

    # History

}
