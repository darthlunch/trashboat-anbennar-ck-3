k_sorncost = {
	1000.1.1 = { change_development_level = 8 }
	983.5.1 = {
		holder = 511	#Petran of Sorncost
	}
	1019.2.5 = {
		holder = 8	#Alcuin sil Sorncost
	}
}

d_sorncost = {
	1000.1.1 = { change_development_level = 8 }
}

c_sorncost = {
	1000.1.1 = { change_development_level = 12 }
	983.5.1 = {
		holder = 511	#Petran of Sorncost
	}
	1019.2.5 = {
		holder = 8	#Alcuin sil Sorncost
	}
}

c_fioncavin = {
	1000.1.1 = { change_development_level = 11 }
	979.7.13 = {
		holder = uinteios_0001
	}
	984.1.1 = {
		liege = k_sorncost
	}
}

d_sormanni_hills = {
	1000.1.1 = { change_development_level = 7 }
}

c_aishill = {
	1000.1.1 = { change_development_level = 9 }
	952.1.1 = {
		holder = sorboduos_0002
	}
	972.5.5 = {
		holder = sorboduos_0004
	}
	1021.1.26 = {
		holder = sorboduos_0006
		liege = k_sorncost
	}
}
d_coruan = {
	950.6.17 = {
		holder = aremornios_0002
	}
	994.10.5 = {
		holder = aremornios_0005
	}
	1013.10.3 = {
		holder = aremornios_0007
		liege = k_sorncost
	}
}

c_coruan = {
	1000.1.1 = { change_development_level = 9 }
	950.6.17 = {
		holder = aremornios_0002
	}
	994.10.5 = {
		holder = aremornios_0005
	}
	1013.10.3 = {
		holder = aremornios_0007
		liege = k_sorncost
	}
}

c_ebenuel = {
	1000.1.1 = { change_development_level = 8 }
	950.6.17 = {
		holder = aremornios_0002
	}
	994.10.5 = {
		holder = aremornios_0005
	}
	1013.10.3 = {
		holder = aremornios_0007
		liege = k_sorncost
	}
}

d_venail = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		succession_laws = { elven_elective_succession_law elf_only }
		holder = 40004
		liege = k_sorncost
	}
}

c_venail = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.31 = {
		holder = 40004
		liege = k_sorncost
	}
}

c_firstsight = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		holder = 40004
		liege = d_venail
	}
}

c_edilliande = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		holder = 40005
		liege = d_venail
	}
}