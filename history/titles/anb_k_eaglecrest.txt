
k_eaglecrest = {
	1000.1.1 = {
		change_development_level = 8
	}
	795.9.12 = {
		holder = eaglecrest_0010 # Stovan I "the Foolish" of Eaglecrest
	}
	826.10.17 = {
		holder = 0
	}
	981.10.23 = {
		holder = eaglecrest_0027 # Edmund I of Eaglecrest
	}
	992.12.10 = {
		holder = eaglecrest_0028 # Brandon I of Eaglecrest
	}
	1010.4.2 = {
		holder = eaglecrest_0029 # Warde I of Eaglecrest
	}
}

d_dragonhills = {
	610.7.21 = {
		liege = k_gawed
		holder = eaglecrest_0001 # Wyla I of Eaglecrest
	}
	635.9.29 = {
		holder = eaglecrest_0002 # Godrac I of Eaglecrest
	}
	669.11.2 = {
		holder = eaglecrest_0003 # Welyam I of Eaglecrest
	}
	684.10.5 = {
		holder = eaglecrest_0005 # Edmund I of Eaglecrest
	}
	705.6.26 = {
		holder = eaglecrest_0006 # Rycan I of Eaglecrest
	}
	754.9.19 = {
		holder = eaglecrest_0007 # Edmund II of Eaglecrest
	}
	757.4.2 = {
		holder = eaglecrest_0008 # Rycan II of Eaglecrest
	}
	774.6.9 = {
		holder = eaglecrest_0009 # Stovan I of Eaglecrest
	}
	792.11.20 = {
		holder = eaglecrest_0010 # Stovan II "the Foolish" of Eaglecrest
	}
	795.9.12 = {
		liege = k_eaglecrest
	}
	826.10.17 = {
		liege = k_gawed
		holder = eaglecrest_0012 # Warde I of Eaglecrest
	}
	832.1.11 = {
		holder = eaglecrest_0013 # Godrac III of Eaglecrest
	}
	860.3.14 = {
		holder = eaglecrest_0014 # Godrac IV of Eaglecrest
	}
	865.7.9 = {
		holder = eaglecrest_0017 # Edmund III of Eaglecrest
	}
	900.1.30 = {
		holder = eaglecrest_0018 # Welyam II of Eaglecrest
	}
	907.6.12 = {
		holder = eaglecrest_0019 # Brandon I of Eaglecrest
	}
	933.7.17 = {
		holder = eaglecrest_0020 # Welyam III of Eaglecrest
	}
	940.4.3 = {
		holder = eaglecrest_0021 # Alenn I of Eaglecrest
	}
	962.11.14 = {
		holder = eaglecrest_0026 # Warde II of Eaglecrest
	}
	980.4.29 = {
		liege = k_eaglecrest
	}
	981.10.23 = {
		holder = eaglecrest_0027 # Edmund I of Eaglecrest
	}
	992.12.10 = {
		holder = eaglecrest_0028 # Brandon II of Eaglecrest
	}
	1010.4.2 = {
		holder = eaglecrest_0029 # Warde III of Eaglecrest
	}
}

c_eaglecrest = {
	1000.1.1 = {
		change_development_level = 10
	}
	
	610.7.21 = {
		holder = eaglecrest_0001 # Wyla I of Eaglecrest
	}
	635.9.29 = {
		holder = eaglecrest_0002 # Godrac I of Eaglecrest
	}
	669.11.2 = {
		holder = eaglecrest_0003 # Welyam I of Eaglecrest
	}
	684.10.5 = {
		holder = eaglecrest_0005 # Edmund I of Eaglecrest
	}
	705.6.26 = {
		holder = eaglecrest_0006 # Rycan I of Eaglecrest
	}
	754.9.19 = {
		holder = eaglecrest_0007 # Edmund II of Eaglecrest
	}
	757.4.2 = {
		holder = eaglecrest_0008 # Rycan II of Eaglecrest
	}
	774.6.9 = {
		holder = eaglecrest_0009 # Stovan I of Eaglecrest
	}
	792.11.20 = {
		holder = eaglecrest_0010 # Stovan II "the Foolish" of Eaglecrest
	}
	826.10.17 = {
		holder = eaglecrest_0012 # Warde I of Eaglecrest
	}
	832.1.11 = {
		holder = eaglecrest_0013 # Godrac III of Eaglecrest
	}
	860.3.14 = {
		holder = eaglecrest_0014 # Godrac IV of Eaglecrest
	}
	865.7.9 = {
		holder = eaglecrest_0017 # Edmund III of Eaglecrest
	}
	900.1.30 = {
		holder = eaglecrest_0018 # Welyam II of Eaglecrest
	}
	907.6.12 = {
		holder = eaglecrest_0019 # Brandon I of Eaglecrest
	}
	933.7.17 = {
		holder = eaglecrest_0020 # Welyam III of Eaglecrest
	}
	940.4.3 = {
		holder = eaglecrest_0021 # Alenn I of Eaglecrest
	}
	962.11.14 = {
		holder = eaglecrest_0026 # Warde II of Eaglecrest
	}
	980.4.29 = {
		holder = eaglecrest_0027 # Edmund III of Eaglecrest
	}
	992.12.10 = {
		holder = eaglecrest_0028 # Brandon II of Eaglecrest
	}
	1010.4.2 = {
		holder = eaglecrest_0029 # Warde III of Eaglecrest
	}
}

c_lodan_crags = {
	1000.1.1 = {
		change_development_level = 6
	}
	
	610.7.21 = { 
		liege = d_dragonhills
	}
	
	975.1.2 = {
		holder = lodan_0002 #Carl Lodan
	}
	1007.5.15 = {
		holder = lodan_0001 #Humbar Lodan
	}
}

c_feycombe = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_south_floodmarsh = {
	1000.1.1 = {
		change_development_level = 6
	}
	
	610.7.21 = {
		liege = d_dragonhills
	}
	
	975.1.1 = {
		holder = floodmark_0001 #Conric Floodmark
	}
}

c_north_floodmarsh = {
	1000.1.1 = {
		change_development_level = 6
	}
}
