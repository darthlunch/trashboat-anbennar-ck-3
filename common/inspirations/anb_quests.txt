﻿inspiration_goblin_quest = {
	gold = { value = basic_fund_inspiration_cost }
	
	progress_chance = adventure_progress_chance_svalue

	# Root = inspiration
	# scope:inspiration = inspiration
	# scope:inspiration_owner = inspiration owner
	# scope:inspiration_sponsor = inspiration sponsor

	on_creation = {
		set_variable = {
			name = inspiration_quest_type
			value = flag:goblin_quest
		}
		if = {
			limit = { NOT = { exists = scope:inspiration_sponsor } }
			scope:inspiration_owner = {
				save_scope_as = inspiration_sponsor
				var:quest_location = {
					save_scope_as = quest_location
				}
			}
		}
	}

	on_complete = {
		# Remove the quest modifier
	}

	on_monthly = {
		# Quest events here, these fire for the owner - normally the player
		scope:inspiration_sponsor = {
			trigger_event = {
				on_action = on_goblin_adventure_maintenance # TODO
			}
			trigger_event = {
				on_action = on_goblin_adventure_maintenance_events # TODO
			}
		}
	}

	on_sponsor = {
		# Quest events here, these fire for the PLAYER
	}

	on_owner_death = {
		# Normally do nothing
	}

	is_valid = {
		# Check that quest modifier still remains
	}

	on_invalidated = {
		# Normally do nothing
	}

	is_sponsor_valid = {
	}

	on_sponsor_invalidated = {
		# Normally do nothing
	}

	can_sponsor = {
	}
}
