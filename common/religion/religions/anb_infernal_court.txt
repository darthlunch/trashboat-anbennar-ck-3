﻿infernal_court_religion = { 
	family = rf_pantheonic

	doctrine = infernal_courts_hostility_doctrine 

	#Main Group
	doctrine = doctrine_no_head
	doctrine = doctrine_gender_male_dominated
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_lay_clergy

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_restricted

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_accepted
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_accepted

	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_cremation

	traits = {	#TODO
		virtues = {
			wrathful
			vengeful
			arrogant
		}
		sins = {
			calm
			forgiving
			humble
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_followers_of_arjuna" }
		{ name = "holy_order_faith_maharatas" }
		{ name = "holy_order_vyuha_of_highgod" }
		{ name = "holy_order_vyuha_of_the_temple_of_place" }
		{ name = "holy_order_maharatas_of_highgod" }
	}

	holy_order_maa = { horse_archers } #TODO

	#TODO
	localization = {
		HighGodName = infernal_court_high_god_name
		HighGodName2 = infernal_court_high_god_name
		HighGodNamePossessive = infernal_court_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = infernal_court_high_god_name_alternate
		HighGodNameAlternatePossessive = infernal_court_high_god_name_alternate_possessive

		#Creator
		CreatorName = infernal_court_creator_god_name
		CreatorNamePossessive = infernal_court_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = infernal_court_health_god_name
		HealthGodNamePossessive = infernal_court_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = infernal_court_fertility_god_name
		FertilityGodNamePossessive = infernal_court_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = infernal_court_wealth_god_name
		WealthGodNamePossessive = infernal_court_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod
		HouseholdGodName = infernal_court_household_god_name
		HouseholdGodNamePossessive = infernal_court_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = infernal_court_fate_god_name
		FateGodNamePossessive = infernal_court_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod
		KnowledgeGodName = infernal_court_knowledge_god_name
		KnowledgeGodNamePossessive = infernal_court_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = infernal_court_war_god_name
		WarGodNamePossessive = infernal_court_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = infernal_court_trickster_god_name
		TricksterGodNamePossessive = infernal_court_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = infernal_court_night_god_name
		NightGodNamePossessive = infernal_court_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = infernal_court_water_god_name
		WaterGodNamePossessive = infernal_court_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM


		PantheonTerm = religion_the_gods
		PantheonTerm2 = religion_the_gods_2
		PantheonTerm3 = religion_the_gods_3
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { infernal_court_high_god_name infernal_court_fertility_god_name infernal_court_wealth_god_name infernal_court_fate_god_name infernal_court_knowledge_god_name infernal_court_war_god_name infernal_court_trickster_god_name infernal_court_night_god_name }
		DevilName = infernal_court_devil_name
		DevilNamePossessive = infernal_court_devil_name_possessive
		DevilSheHe = infernal_court_devil_shehe
		DevilHerHis = infernal_court_devil_herhis
		DevilHerselfHimself = infernal_court_devil_herselfhimself
		EvilGodNames = { infernal_court_devil_name }
		HouseOfWorship = infernal_court_house_of_worship
		HouseOfWorship2 = infernal_court_house_of_worship
		HouseOfWorship3 = infernal_court_house_of_worship
		HouseOfWorshipPlural = infernal_court_house_of_worship_plural
		ReligiousSymbol = infernal_court_religious_symbol
		ReligiousSymbol2 = infernal_court_religious_symbol
		ReligiousSymbol3 = infernal_court_religious_symbol
		ReligiousText = infernal_court_religious_text
		ReligiousText2 = infernal_court_religious_text
		ReligiousText3 = infernal_court_religious_text
		ReligiousHeadName = infernal_court_religious_head_title
		ReligiousHeadTitleName = infernal_court_religious_head_title_name
		DevoteeMale = infernal_court_devotee_male
		DevoteeMalePlural = infernal_court_devotee_male_plural
		DevoteeFemale = infernal_court_devotee_female
		DevoteeFemalePlural = infernal_court_devotee_female_plural
		DevoteeNeuter = infernal_court_devotee_neuter
		DevoteeNeuterPlural = infernal_court_devotee_neuter_plural
		PriestMale = infernal_court_priest
		PriestMalePlural = infernal_court_priest_plural
		PriestFemale = infernal_court_priest_female
		PriestFemalePlural = infernal_court_priest_female_plural
		PriestNeuter = infernal_court_priest
		PriestNeuterPlural = infernal_court_priest_plural
		AltPriestTermPlural = infernal_court_priest_term_plural
		BishopMale = infernal_court_bishop
		BishopMalePlural = infernal_court_bishop_plural
		BishopFemale = infernal_court_bishop_female
		BishopFemalePlural = infernal_court_bishop_female_plural
		BishopNeuter = infernal_court_bishop
		BishopNeuterPlural = infernal_court_bishop_plural
		DivineRealm = infernal_court_divine_realm
		DivineRealm2 = infernal_court_divine_realm
		DivineRealm3 = infernal_court_divine_realm
		PositiveAfterLife = infernal_court_divine_realm
		PositiveAfterLife2 = infernal_court_divine_realm
		PositiveAfterLife3 =infernal_court_divine_realm
		NegativeAfterLife = infernal_court_negative_afterlife
		NegativeAfterLife2 = infernal_court_negative_afterlife
		NegativeAfterLife3 = infernal_court_negative_afterlife
		DeathDeityName = paganism_death_deity_name
		DeathDeityNamePossessive = paganism_death_deity_name_possessive
		DeathDeitySheHe = paganism_devil_shehe
		DeathDeityHerHis = paganism_death_deity_herhis
		DeathDeityHerHim = CHARACTER_HERHIM_IT
		
		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_wars
	}	

	faiths = {
		cult_of_agrados = {
			color = { 158 2 2 }
			icon = the_dame

			# TODO
			holy_site = aldtempel
			holy_site = cannwic

			doctrine = tenet_pursuit_of_power
			doctrine = tenet_unrelenting_faith
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
		}
		
		cult_of_hedine = {
			color = { 128 0 255 }
			icon = the_dame

			# TODO
			holy_site = esmaraine

			doctrine = tenet_carnal_exaltation
			doctrine = tenet_hedonistic
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
			
			#Main Group
			doctrine = doctrine_gender_equal

			#Marriage
			doctrine = doctrine_polygamy
			doctrine = doctrine_consanguinity_unrestricted

			#Crimes
			doctrine = doctrine_homosexuality_accepted
			doctrine = doctrine_adultery_men_accepted
			doctrine = doctrine_adultery_women_accepted
			doctrine = doctrine_kinslaying_shunned
			doctrine = doctrine_deviancy_virtuous

			#Clerical Functions
			doctrine = doctrine_clerical_function_alms_and_pacification
			
			localization = {
				HighGodName = cult_of_hedine_high_god_name
				HighGodName2 = cult_of_hedine_high_god_name
				HighGodNamePossessive = cult_of_hedine_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_SHE
				HighGodHerselfHimself = CHARACTER_HERSELF
				HighGodHerHis = CHARACTER_HERHIS_HER
				HighGodNameAlternate = cult_of_hedine_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_hedine_high_god_name_alternate_possessive
			}
		}
		
		cult_of_forsara = {
			color = { 255 255 0 }
			icon = the_dame
			
			# TODO
			holy_site = damescrown

			doctrine = tenet_sacred_shadows
			doctrine = tenet_risk_and_reward
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
			
			#Main Group
			doctrine = doctrine_gender_equal

			#Marriage
			doctrine = doctrine_concubines

			#Crimes
			doctrine = doctrine_adultery_men_accepted
			doctrine = doctrine_adultery_women_accepted
			doctrine = doctrine_kinslaying_shunned

			#Clerical Functions
			doctrine = doctrine_clerical_function_alms_and_pacification
			
			localization = {
				HighGodName = cult_of_forsara_high_god_name
				HighGodName2 = cult_of_forsara_high_god_name
				HighGodNamePossessive = cult_of_forsara_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_SHE
				HighGodHerselfHimself = CHARACTER_HERSELF
				HighGodHerHis = CHARACTER_HERHIS_HER
				HighGodNameAlternate = cult_of_forsara_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_forsara_high_god_name_alternate_possessive
			}
		}
		
		cult_of_ildran = {
			color = { 128 128 128 }
			icon = the_dame

			# TODO
			holy_site = north_citadel

			doctrine = tenet_religious_legal_pronouncements
			doctrine = tenet_gnosticism
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
			
			#Main Group
			doctrine = doctrine_gender_equal

			#Marriage
			doctrine = doctrine_divorce_approval
			doctrine = doctrine_bastardry_legitimization

			#Crimes
			doctrine = doctrine_witchcraft_virtuous

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			
			localization = {
				HighGodName = cult_of_ildran_high_god_name
				HighGodName2 = cult_of_ildran_high_god_name
				HighGodNamePossessive = cult_of_ildran_high_god_name_possessive
				HighGodNameAlternate = cult_of_ildran_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_ildran_high_god_name_alternate_possessive
			}
		}
		
		cult_of_kazraiel = {
			color = { 255 0 0 }
			icon = the_dame

			# TODO
			holy_site = aldtempel

			doctrine = tenet_warmonger
			doctrine = tenet_sacrificial_ceremonies
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist

			#Clerical Functions
			doctrine = doctrine_clerical_gender_male_only
			
			localization = {
				HighGodName = cult_of_kazraiel_high_god_name
				HighGodName2 = cult_of_kazraiel_high_god_name
				HighGodNamePossessive = cult_of_kazraiel_high_god_name_possessive
				HighGodNameAlternate = cult_of_kazraiel_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_kazraiel_high_god_name_alternate_possessive
			}
		}
		
		cult_of_canturian = {
			color = { 0 255 0 }
			icon = the_dame

			# TODO
			holy_site = toarnaire

			doctrine = tenet_gruesome_festivals
			doctrine = tenet_human_supremacy
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist

			#Marriage
			doctrine = doctrine_concubines

			#Crimes
			doctrine = doctrine_deviancy_accepted

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			
			localization = {
				HighGodName = cult_of_canturian_high_god_name
				HighGodName2 = cult_of_canturian_high_god_name
				HighGodNamePossessive = cult_of_canturian_high_god_name_possessive
				HighGodNameAlternate = cult_of_canturian_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_canturian_high_god_name_alternate_possessive
			}
		}
		
		cult_of_ibberal = {
			color = { 255 165 0 }
			icon = the_dame

			# TODO
			holy_site = old_damenath
			holy_site = vertesk

			doctrine = tenet_esotericism
			doctrine = tenet_ritual_cannibalism
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
			
			#Main Group
			doctrine = doctrine_gender_equal

			#Crimes
			doctrine = doctrine_deviancy_accepted
			doctrine = doctrine_witchcraft_virtuous

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			
			localization = {
				HighGodName = cult_of_ibberal_high_god_name
				HighGodName2 = cult_of_ibberal_high_god_name
				HighGodNamePossessive = cult_of_ibberal_high_god_name_possessive
				HighGodNameAlternate = cult_of_ibberal_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_ibberal_high_god_name_alternate_possessive
			}
		}
		
		cult_of_mesner = {
			color = { 0 0 255 }
			icon = the_dame

			# TODO
			holy_site = varillen

			doctrine = tenet_adorcism
			doctrine = tenet_aniconism
			doctrine = tenet_infernal_teachings
			
			doctrine = doctrine_hidden_infernalist
			
			#Main Group
			doctrine = doctrine_gender_equal

			#Marriage
			doctrine = doctrine_concubines

			#Crimes
			doctrine = doctrine_kinslaying_shunned

			#Clerical Functions
			doctrine = doctrine_clerical_function_alms_and_pacification
			
			#Allow pilgrimages
			doctrine = doctrine_pilgrimage_forbidden
			
			localization = {
				HighGodName = cult_of_mesner_high_god_name
				HighGodName2 = cult_of_mesner_high_god_name
				HighGodNamePossessive = cult_of_mesner_high_god_name_possessive
				HighGodNameAlternate = cult_of_mesner_high_god_name_alternate
				HighGodNameAlternatePossessive = cult_of_mesner_high_god_name_alternate_possessive
			}
		}
	}
}