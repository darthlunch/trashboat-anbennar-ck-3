﻿redscale = {
	color = { 152  30  52 }
	
	ethos = ethos_egalitarian
	heritage = heritage_kobold
	language = language_kobold
	martial_custom = martial_custom_equal
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_kobold
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { kobold_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = kobold_redscale
	}
}

darkscale = {
	color = { 112  20  22 }
	
	ethos = ethos_egalitarian
	heritage = heritage_kobold
	language = language_kobold
	martial_custom = martial_custom_equal
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_kobold
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { kobold_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = kobold_darkscale
	}
}

bluescale = {
	color = { 30  52  152 }
	
	ethos = ethos_egalitarian
	heritage = heritage_kobold
	language = language_kobold
	martial_custom = martial_custom_equal
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_kobold
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { kobold_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = kobold_bluescale
	}
}

greenscale = {
	color = { 30  152  52 }
	
	ethos = ethos_egalitarian
	heritage = heritage_kobold
	language = language_kobold
	martial_custom = martial_custom_equal
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_kobold
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { kobold_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = kobold_greenscale
	}
}
