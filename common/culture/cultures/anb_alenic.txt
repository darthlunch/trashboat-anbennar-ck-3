﻿
blue_reachman = {
	color = { 80 107 232 }
	created = 700.1.1
	parents = { dalric old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_reachman_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_parochialism
		tradition_stalwart_defenders
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		25 = caucasian_blond
		5 = caucasian_ginger
		10 = caucasian_brown_hair
		60 = caucasian_dark_hair
	}
}

old_alenic = {
	color = { 82  130  152 }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_alenic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_old_alenic
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = caucasian_blond
		10 = caucasian_brown_hair
		60 = caucasian_dark_hair
	}
}

gawedi = {
	color = "gawedi_blue"
	created = 400.1.1
	parents = { old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_alenic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_strength_in_numbers
		tradition_martial_admiration
		tradition_forest_fighters
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		15 = caucasian_blond
		10 = caucasian_brown_hair
		75 = caucasian_dark_hair
	}
}

moorman = {
	color = { 71  84  102 }
	created = 470.8.1	#actual canon date according to dragonwake
	parents = { old_alenic }
	
	ethos = ethos_communal
	heritage = heritage_alenic
	language = language_alenic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_wetlanders
		tradition_collective_lands
	}
	dlc_tradition = {
		trait = tradition_staunch_traditionalists
		requires_dlc_flag = hybridize_culture
	}
	
	name_list = name_list_moorman
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		10 = caucasian_brown_hair
		70 = caucasian_dark_hair
	}
}

vertesker = {
	color = { 57  81  87 }
	created = 500.1.1
	parents = { gawedi arami }
	
	ethos = ethos_bureaucratic
	heritage = heritage_alenic
	language = language_castanorian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_wetlanders
		tradition_city_keepers
		tradition_maritime_mercantilism
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		5 = caucasian_blond
		15 = caucasian_brown_hair
		80 = caucasian_dark_hair
	}
}

wexonard = {
	color = { 97  0  137 }
	created = 474.7.1	#canon
	parents = { old_alenic }
	
	ethos = ethos_bellicose
	heritage = heritage_alenic
	language = language_alenic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hard_working
		tradition_spartan
		tradition_ruling_caste
	}
	dlc_tradition = {
		trait = tradition_fp1_trials_by_combat
		requires_dlc_flag = the_northern_lords
	}
	
	name_list = name_list_wexonard
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		50 = caucasian_blond
		25 = caucasian_brown_hair
		25 = caucasian_dark_hair
	}
}

marrodic = {
	color = { 157 121 115 }
	created = 492.8.1	#canon
	parents = { old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_alenic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_isolationist
		tradition_mountain_homes
		tradition_spartan
	}
	
	name_list = name_list_old_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		15 = caucasian_blond
		10 = caucasian_brown_hair
		75 = caucasian_dark_hair
	}
}

boundsman = {
	color = { 43 107 191 } # TODO
	created = 592.8.1 # Random date
	parents = { old_alenic morbani }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_rohibonic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_fp2_malleable_subjects
		tradition_life_is_just_a_joke
	}
	
	name_list = name_list_old_alenic

	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = { # TODO
		10 = caucasian_blond
		5 = caucasian_brown_hair
		35 = caucasian_dark_hair
		30 = slavic_dark_hair
		10 = slavic_brown_hair
		15 = caucasian_blond
		10 = caucasian_brown_hair
		75 = caucasian_dark_hair
	}
}